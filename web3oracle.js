var Web3 = require('web3');
//provider = new Web3.providers.HttpProvider("http://localhost:7545");
provider = new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/486eb31222ab427d9a80d5efa33280ce");
web3 = new Web3(provider);
var blocknumber = ''

//daisweepercontractAddress = '0x217C2EeD3aD5c7d94704Ce826b12ae4755fE8380';
//oraclecontractAddress = '0x217C2EeD3aD5c7d94704Ce826b12ae4755fE8380';


//daisweeperContract = web3.eth.contract(contractABI).at(daisweepercontractAddress);
//oracleContract = web3.eth.contract(contractABI).at(oraclecontractAddress);



// function that creates a endless loo looking for block heigh to increment
function getCurrentblock(callback) {
    web3.eth.getBlockNumber(function(error, result){
        if(!error) {
            if (blocknumber != result) {
                blocknumber  = result;
                callback(null,blocknumber);
            }
            // recall the same function for endless loop
            setTimeout(() => {
                getCurrentblock ( function(err, cblock){
                if (err) {
                    console.log("ERROR : ",err);
                }
                else {
                    console.log(cblock);
                    //getPotTotal();
                    // only call every other block
                    var modflag = cblock % 2;
                    if (modflag == 0) {
                        console.log("even block found")
                        getblocktransactions(cblock);
                    }
                }
                });
            }, 2000);
        }
        else {
            console.error(error);
        }
    })
}


// get the transactions on the new block
function getblocktransactions(newblock) {
    web3.eth.getBlock(newblock, function(error, result){
    if(!error) {
        if(result == null) { // fix bug where null block is being returned.
            console.log("returned null block");
        }
        else {
            var trans = result.transactions;
            for (i=0;i < trans.length; i++){
                console.log("trans: "  + trans[i]);
                gettransaction(newblock, trans[i])
            }
        }

    }
    else {
        console.error(error);
    }
    });
};


// get the to each transaction check if it the payable contract
function gettransaction(block, transaction) {
    web3.eth.getTransaction(transaction , function(error, result){
    if(!error){
        console.log("Block: " + block + " Transaction: " + transaction + " To: " + result.to)
    }
    else {
        console.error(error);
    }
    })
}

// start the script
getCurrentblock ( function(err, cblock) {
    if (err) {
      console.log("ERROR : ",err);
    }
    else {
      console.log("Started block watcher successfully")
    }
});