var Web3 = require('web3');
//provider = new Web3.providers.HttpProvider("http://localhost:7545");
provider = new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/486eb31222ab427d9a80d5efa33280ce");
web3 = new Web3(provider);
var blocknumber = ''

contractABI = [
	{
		"constant": true,
		"inputs": [],
		"name": "contractbalance",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "enterPot",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "checkforwinners",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "howmuchbet",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "remainder",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "contestantcount",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "potTotal",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "winnings",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "contestants",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getblockheight",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "admin",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "constructor"
	}
];

//contractAddress = '0x217C2EeD3aD5c7d94704Ce826b12ae4755fE8380'; //contract address
//tokenContract = web3.eth.contract(contractABI).at(contractAddress);




/*
web3.eth.accounts.forEach(account => {
    balance = web3.eth.getBalance(account);
    console.log(balance);
  })

 web3.eth.getBlock(5, function(error, result){
    if(!error)
        console.log(JSON.stringify(result));
    else
        console.error(error);
 })


*/


function getContractOwner() {

  tokenContract.admin(function (error, result) {
  if (!error) {
      console.log(result);
  } else {
      console.error(error);
  }
});
};

function getPotTotal() {

  tokenContract.potTotal(function (error, result) {
  if (!error) {
      console.log("Pot Total: " + result.toString(10)); //converts big number to a string
  } else {
      console.error(error);
  }
});
};



function getblock(block) {

  console.log("test")
 web3.eth.getBlock(block, function(error, result){
  if(!error) {
  console.log(result);
  var trans = result.transactions
  console.log(trans.length)
  }
  else{
  console.error(error);
  }
 })

}


function getblocktransactions() {


 web3.eth.getBlock(7489616, function(error, result){
  if(!error) {
  var trans = result.transactions
    for (i=0;i < trans.length; i++){
      console.log("trans: "  + trans[i])
      gettransaction(trans[i]);

    }
  }
  else{
  console.error(error);
  }
 })

}




function gettransaction(transaction) {

  web3.eth.getTransaction(transaction , function(error, result){
    if(!error){

    console.log(result.to)
    }
    else {
    console.error(error);
    }
  })

  }



  function getCurrentblock(callback) {

    web3.eth.getBlockNumber(function(error, result){
     if(!error) {
     if (blocknumber != result) {
     blocknumber  = result;

     callback(null,blocknumber);
     }
     // recall the same function for endless loop
     setTimeout(() => {
     getCurrentblock ( function(err, cblock){
      if (err) {
        console.log("ERROR : ",err);
      }
      else {
        console.log(cblock);
        //getPotTotal();
        getblock(cblock);

      }
    });
  }, 2000);


     }
     else{
     console.error(error);

     }
    })

   }





getCurrentblock ( function(err, cblock){
  if (err) {
    console.log("ERROR : ",err);
  }
  else {
    console.log("Started block watcher successfully")
  }
});



// getblocktransactions();
