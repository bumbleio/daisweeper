pragma solidity ^0.5.0;

contract DaiSweeper {

address public admin;
mapping(address => uint256) public howmuchbet;
uint256 public potTotal;
uint256 public contestantcount;
address[] public contestants;
uint256 public remainder;
uint256 public winnings;



constructor () public payable {
    // have to deploy this cintract with 1 wei
    require(msg.value == 1, "must deploy contract with 1 wei");
    admin = msg.sender;
    potTotal = msg.value;
    }

 function enterPot() public payable{
        require(msg.value > 0);
        uint256 _amountbet = msg.value;

        // check if user hasnt bet before add to teh contestants array and the count
        if (howmuchbet[msg.sender] == 0) {
            contestants.push(msg.sender);
            contestantcount += 1;
        }

        howmuchbet[msg.sender] = howmuchbet[msg.sender] + _amountbet;
        potTotal += _amountbet;

        // check if the pot total is divisible by 10
        remainder = potTotal % 10;
        if(remainder == 0) {
            uint _winnings = potTotal / contestantcount;
            for (uint i = 0; i < contestants.length; i++) {
                // have to explicity cast address to a payable address to send the winnings
                // https://ethereum.stackexchange.com/questions/64108/whats-the-difference-between-address-and-address-payable
                address payable _winningaddress = address(uint160(contestants[i]));
                require(potTotal >= _winnings, "Not enought money in pot");
                potTotal = potTotal - _winnings;
                contestantcount -= 1;
                howmuchbet[_winningaddress] = 0;
                delete contestants[i];
                _winningaddress.transfer(_winnings);
        }
    }

}

function getbloackheight() public view returns(uint256) {
    uint blocknumber = block.number;
    return blocknumber;
}


function contractbalance() public view returns(uint256) {
    return address(this).balance;
}







}